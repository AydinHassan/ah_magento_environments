# Ah Magento Environments
This extension allows you to set your applications environments via a config file. It provides two static helper methods:


## Ah_Environment_Helper_Data::isEnvironment($env);

The above will look through your specified config and attempt to match the current environment with either one of your configured dev or production environments.

To elaborate the config has a the concept of two types of environments: dev & production. For this tool staging and production environments are set as "production", local machines and dev boxes would be set as "dev".

Therefore `isEnvironment('dev')` would return true if it matches the current host to either your local machine config or the dev box config.

    Example config:

        <default>
            <app_environments>
                <force_production>false</force_production>

                <environments>
                    <dev_environments>
                        <local>site.local</local>
                        <dev>site.dev.com</dev>
                    </dev_environments>

                    <production_environments>
                        <staging>site.staging.com</staging>
                        <production>site.com</production>
                    </production_environments>
                </environments>
            </app_environments>
        </default>

If you're current environment was site.dev.com or site.local, `isEnvironment('dev')` would return true. If the current host was one of the hosts listen under <production_environments> then it would return false

The <force_production> flag will, if set to true, will make `isEnvironment('dev')` return false even if you are on a dev environment. This is useful for example when want to test production assets. Eg:

    if(Ah_Environment_Helper_Data::isEnvironment('dev')) {
        <script src="<?php echo $this->getSkinUrl('assets/js/vendor/require.min.js') ?>"></script>
    } else {
        <script src="<?php echo $this->getSkinUrl('assets/js/dist/modules.combined.min.js') ?>"></script>
    }

This example shows how you output minified JavaScript only in production environments, using the <force_production> flag you can output the minified assets on a dev environment to test they are working



## Ah_Environment_Helper_Data::detectEnvironment();

This function will return either "dev" or "production" depending on the configuration described above.

It will internally check configured enviroments against `$_SERVER['HTTP_HOST']` or `$_SERVER['SERVER_NAME']` preferring `$_SERVER['HTTP_HOST']`. It also allows for an optional callback which you can use
to return a different value, for example:

   Ah_Environment_Helper_Data::detectEnvironment(function() {
       return $_SERVER['MY_ENV'];
   });

   Note: The above was inspired by Laravel: http://laravel.com/docs/configuration#environment-configuration


## Setup
To setup - uncomment the example config and replace with your real values!


### Assumptions

If no environment is configured for the current host, calling `detectEnvironment();` will return `dev` and `isEnvironment('dev')` will return `true`


## Installation

This module is installable via `Composer`. If you have used the Magento Skeleton as a base module then you can just require this project and all the rest is done for you. 

```
$ cd project-root
$ ./composer.phar require "aydinhassan/ah_magento_environments"
```

Note: As these repositories are currently private and not available via a public package list like [Packagist](https://packagist.org/) Or [Firegento](http://packages.firegento.com") you need to add the repository to the projects `composer.json` before you require the project.

```
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:AydinHassan/ah_magento_environments.git"
    }
],
```

If you have not used the Magento Skeleton Application as a base you will need to install `Composer` and depending on whether it is a Magento project or standard PHP project some different steps are required. I'm going to omit these details as this is a Magento extension and you should probably be using the JH Magento Skeleton Application


## Generating Docs

Move to the cloned repository location, usually in the `vendor` directory of your Magento project.
Install composer dependencies and then run phpDocumentor2.

```
$ cd vendor/aydinhassan/ah_magento_environments
$ php composer.phar install --dev
$ vendor/bin/phpdoc.php -d src -t docs
```

The docs will be available in the ./docs directory!

## Running tests

Coming soon...