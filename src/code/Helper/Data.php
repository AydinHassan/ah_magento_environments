<?php

/**
 * Class Ah_Environment_Helper_Data
 * @author Aydin Hassan <aydin@wearejh.com>
 */
class Ah_Environment_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Dev env key
     */
    const DEV_ENV = 'dev';

    /**
     * Prod env key
     */
    const PRODUCTION_ENV= 'production';

    /**
     * Check whether the current environment is a dev or production style env
     * Environments are configured in the config.
     * force_production config flag will make this function return false if in a dev environment
     *
     * @param string $env
     * @return bool
     */
    public static function isEnvironment($env)
    {
        if(($env === self::DEV_ENV && Mage::getStoreConfig('app_environments/force_production') === 'true') ) {
            return false;
        }

        return ($env === self::detectEnvironment());
    }

    /**
     * @param null|callable $callback
     * @return string
     */
    public static function detectEnvironment($callback = null) {

        $confirguredEnvironments    = Mage::getStoreConfig('app_environments/environments');
        $devEnironments             = $confirguredEnvironments[self::DEV_ENV . "_environments"];
        $productionEnironments      = $confirguredEnvironments[self::DEV_ENV . "_environments"];

        if(!is_callable($callback)) {
            $env = ($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
        } else {
            $env = call_user_func($callback);
        }

        /* If current host is in dev enviroments array return dev */
        if(is_array($devEnironments) && in_array($env, $devEnironments)) {
            return self::DEV_ENV;
        /* If current host is in production enviroments array return production */
        } elseif (is_array($productionEnironments) && in_array($env, $productionEnironments)) {
            return self::PROD_ENV;
        /* If host is unknown assume dev */
        } else {
            return self::DEV_ENV;
        }
    }

}